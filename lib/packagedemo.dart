library packagedemo;

import 'package:flutter/material.dart';
import 'package:packagedemo/widget/custom_text.dart';
import 'package:packagedemo/widget/custom_textfield.dart';

class CardDetailScreen extends StatefulWidget {
  @override
  _CardDetailScreenState createState() => _CardDetailScreenState();
}

class _CardDetailScreenState extends State<CardDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          "Card Details",
          color: Colors.white,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CustomText(
                "Card Details",
                fontSize: 14,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: CustomTextField(
                  "Card Number", "Card Number", TextEditingController()),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: CustomTextField(
                  "Valid Till", "mm/yy", TextEditingController()),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: CustomTextField("CVV", "CVV", TextEditingController()),
            )
          ],
        ),
      ),
    );
  }
}

// class CustomAlertBox {
//   /// Bu şekilde döküman yorumları oluşturabilirsiniz kullanan kişiler için faydalı olur.
//   static Future showCustomAlertBox({
//     @required BuildContext context,
//     @required Widget willDisplayWidget,
//   }) {
//     assert(context != null, "context is null!!");
//     assert(willDisplayWidget != null, "willDisplayWidget is null!!");
//     return showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.circular(15)),
//             ),
//             content: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 willDisplayWidget,
//                 MaterialButton(
//                   color: Colors.white30,
//                   child: Text('close alert'),
//                   onPressed: () {
//                     Navigator.of(context).pop();
//                   },
//                 )
//               ],
//             ),
//             elevation: 10,
//           );
//         });
//   }

//   static Widget wwww({
//     @required BuildContext context,
//     @required Widget willDisplayWidget,
//   }) {
//     assert(context != null, "context is null!!");
//     assert(willDisplayWidget != null, "willDisplayWidget is null!!");
//     return showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.circular(15)),
//             ),
//             content: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 willDisplayWidget,
//                 MaterialButton(
//                   color: Colors.white30,
//                   child: Text('close alert'),
//                   onPressed: () {
//                     Navigator.of(context).pop();
//                   },
//                 )
//               ],
//             ),
//             elevation: 10,
//           );
//         });
//   }
// }
