import 'package:flutter/cupertino.dart';

class ColorResource {
  static const Color color616267 = Color(0xff616267);
  static const Color colorb9b9bf = Color(0xffb9b9bf);
  static const Color colorf5f5f7 = Color(0xfff5f5f7);
  static const Color color1c1d22 = Color(0xff1c1d22);
}
